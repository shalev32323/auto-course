import { defineConfig } from 'cypress'

export default defineConfig({
  watchForFileChanges: true,
  defaultCommandTimeout: 15000,
  viewportHeight: 660,
  viewportWidth: 1200,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    baseUrl: 'https://www.imdb.com',
    specPattern: 'cypress/tests/**/*.cy.{js,jsx,ts,tsx}',
  },
})
