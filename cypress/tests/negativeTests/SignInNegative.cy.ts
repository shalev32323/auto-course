import { UserLogin } from "../../infoObj/signInObj";
import { Home } from "../../pages/homePage";
import { SignIn } from "../../pages/SignInPage";

describe('sign in', () => {
  var userData: UserLogin;
  const signIn = new SignIn();
  const home = new Home();

  before("intro login", () => {
    cy.fixture('signIn.json').then((user) => {
      userData = user.badUser;
    })
    signIn.goToPage();
    home.goToLoginPage();
    signIn.signInWithImdb();
  });

  it('bad sign in', () => {
    signIn.signIn(userData)
  });

  after("validate login", () => {
    signIn.validateError()
  });
});
