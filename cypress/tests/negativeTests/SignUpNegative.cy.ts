import { UserInfo } from "../../infoObj/signUpObj";
import { SignUp } from "../../pages/SignUpPage";

describe('sign up', () => {
  var userData: UserInfo;
  const signUp = new SignUp();

  before("intro sign up", () => {
    cy.fixture('signUp.json').then((user) => {
      userData = user;
    })
    signUp.goToPage();
  })

  it('good sign up', () => {
    signUp.badSignUp(userData);
  });

  after("validate error", () => {
    signUp.validateEmailError();
  })

});
