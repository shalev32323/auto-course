import { UserLogin } from "../../infoObj/signInObj";
import { Home } from "../../pages/homePage";
import { SignIn } from "../../pages/SignInPage";

describe('watch list', () => {
  var userData: UserLogin;
  var movies: string[];
  const home = new Home();
  const signIn = new SignIn();

  before("load data", () => {
    cy.fixture('signIn.json').then((user) => {
      userData = user.goodUser;
    })
    cy.fixture("watchList.json").then((list) => {
      movies = list.moviesInMyList;
    })
    home.goToPage();
    home.goToWatchList();
    signIn.signInWithImdb();
  });

  beforeEach("login", () => {
    home.goToPage();
    home.goToWatchList();
    signIn.signInWithImdb();
    signIn.signIn(userData);
  });

  it('validate watch list names', () => {
    home.checkMoviesNames(movies)
  });
});
