import { UserLogin } from "../../infoObj/signInObj";
import { Home } from "../../pages/homePage";
import { SignIn } from "../../pages/SignInPage";

describe('sign in', () => {
  var userData: UserLogin;
  const signIn = new SignIn();
  const home = new Home();

  before("intro login", () => {
    cy.fixture('signIn.json').then((user) => {
      userData = user.goodUser;
    })
    signIn.goToPage();
    home.goToLoginPage();
    signIn.signInWithImdb();
  });

  it('good sign in', () => {
    signIn.signIn(userData)
  });

  after("validate login", () => {
    signIn.validateLogin();
  });
});
