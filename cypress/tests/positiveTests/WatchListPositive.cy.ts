import { UserLogin } from "../../infoObj/signInObj";
import { Home } from "../../pages/homePage";
import { SignIn } from "../../pages/SignInPage";
import { WatchList } from "../../pages/WatchListPage";

describe('watch list', () => {
  var userData: UserLogin;
  const home = new Home();
  const signIn = new SignIn();
  const watchList = new WatchList();

  before("load data", () => {
    cy.fixture('signIn.json').then((user) => {
      userData = user.goodUser;
    })
  });
 

  beforeEach("login", () => {
    signIn.goToPage();
    home.goToLoginPage();
    signIn.signInWithImdb();
    signIn.signIn(userData);
  });

  it('add to watchlist and validate', () => {
    watchList.searchMovie();
    watchList.checkSearchResults();
    watchList.addToWatchlist();
  });

  after("validate count of movies", () => {
    home.checkTheCount();
  });
});
