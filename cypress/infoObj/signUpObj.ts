export interface UserInfo {
    usrname: string,
    email: string,
    password: string,
    checkpassword: string,
}