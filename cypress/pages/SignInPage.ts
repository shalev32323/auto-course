import { signInLocators } from "../locators/SignInLocators";
import { BasePage } from './BasePage';
import { UserLogin } from '../infoObj/signInObj';

export class SignIn extends BasePage {

    constructor(url: string = "") {
        super(url);
    }

    signInWithImdb(): void {
        cy.get(signInLocators.signImdb).contains('Sign in with IMDb').click()

    }

    signIn(obj: UserLogin) {
        this.insertemail(obj.email);
        this.insertpassword(obj.password);
        this.rememberMe();
        this.clickLogin();
    }

    insertemail(email: string): void {
        cy.get(signInLocators.emailField).clear().type(email)
    }
    insertpassword(password: string): void {
        cy.get(signInLocators.passwordField).clear().type(password)
    }

    clickLogin(): void {
        cy.get(signInLocators.signInBtn).click();
    }

    validateLogin(): void {
        cy.get(signInLocators.validateLogin).eq(1).should("have.text", "Adar");
    }
    
    rememberMe(): void {
        cy.get(signInLocators.rememberMe).click();
    }
    
    validateError(): void {
        cy.get(signInLocators.errorMessage).should("be.visible");
    }
}