import { UserInfo } from "../infoObj/signUpObj";
import { signUpLocators } from "../locators/SignUpLocators";
import { BasePage } from './BasePage';

export class SignUp extends BasePage {
    private name: string = "shalev"
    private pass: string = "12345"
    private email: string = String(Math.floor(Math.random() * 100000) + 1) + "@gmail.com"


    constructor(url: string = "/ap/register?openid.return_to=https%3A%2F%2Fwww.imdb.com%2Fregistration%2Fap-signin-handler%2Fimdb_us&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=imdb_us&openid.mode=checkid_setup&siteState=eyJvcGVuaWQuYXNzb2NfaGFuZGxlIjoiaW1kYl91cyIsInJlZGlyZWN0VG8iOiJodHRwczovL3d3dy5pbWRiLmNvbS8_cmVmXz1sb2dpbiJ9&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&tag=imdbtag_reg-20") {
        super(url);
    }

    badSignUp(obj: UserInfo) {
        this.insertName(obj.usrname);
        this.insertemail(obj.email);
        this.insertpassword(obj.password);
        this.insertcheckPassword(obj.checkpassword);
        this.clickCreateAccount();
    }

    signUp() {
        this.insertName(this.name);
        this.insertemail(this.email);
        this.insertpassword(this.pass);
        this.insertcheckPassword(this.pass);
        this.clickCreateAccount();
    }

    insertName(name: string): void {
        cy.get(signUpLocators.nameField).clear().type(name)
    }
    insertemail(email: string): void {
        cy.get(signUpLocators.emailField).clear().type(email)
    }
    insertpassword(password: string): void {
        cy.get(signUpLocators.passwordField).clear().type(password)
    }
    insertcheckPassword(chPassword: string): void {
        cy.get(signUpLocators.checkPasswordField).clear().type(chPassword)
    }
    clickCreateAccount(): void {
        cy.get(signUpLocators.createAccount).click();
    }
    validateEmailError(): void {
        cy.get(signUpLocators.errorMessage).eq(0).should("be.visible");
    }
}