import { homeLocators } from '../locators/homePageLocators';
import { BasePage } from './BasePage';

export class Home extends BasePage {

    constructor(url: string = "") {
        super(url); 
    }

    goToLoginPage(): void {
        cy.get(homeLocators.barBtns).contains('Sign In').click()
    }

    goToWatchList(): void {
        cy.get(homeLocators.barBtns).contains('Watchlist').click()
    }

    checkMoviesNames(movies: string[]): void {
        cy.get(homeLocators.movieName).each((el, index) => {
            expect(el.text()).to.equal(movies[index])
        })
    }

    checkTheCount(): void {
        cy.reload()
        cy.get(homeLocators.countOfMovies).should("have.text", "6")
    }
}
