import { BasePage } from './BasePage';
import { watchlistLocators } from "../locators/WatchListLocators";

export class WatchList extends BasePage {

    constructor(url: string = "") {
        super(url);
    }

    searchMovie(): void {
        cy.get(watchlistLocators.searchField).clear().type('Soul')
    }

    checkSearchResults(): void {
        cy.get(watchlistLocators.searchResults).eq(0).should('contain', 'Soul').click()
    }

    addToWatchlist(): void {
        cy.contains("Add to Watchlist").click()
    }
}