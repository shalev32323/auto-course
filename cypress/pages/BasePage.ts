export class BasePage {
    private fullUrl: string;

    constructor(pageUrl: string) {
        this.fullUrl = Cypress.config().baseUrl + pageUrl;
    }

    goToPage(): void {
        cy.visit(this.fullUrl)
    }
}