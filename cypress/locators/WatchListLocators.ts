interface watchlist {
    searchField: string,
    searchResults: string,
    addToWatchlistBtn: string,
    checkIfAdded: string,
}

export const watchlistLocators: watchlist = {
    searchField: '#suggestion-search',
    searchResults: '.searchResults__ResultTextItem-sc-1pmqwbq-2.lolMki._26kHO_8bFBduUIYADnVHFY',
    addToWatchlistBtn: '.ipc-button__text',
    checkIfAdded: '.ipc-button'
}

