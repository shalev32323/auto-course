interface signIn {
    signInBtn: string,
    passwordField: string,
    emailField: string,
    signImdb: string,
    validateLogin: string,
    rememberMe: string,
    errorMessage: string
}

export const signInLocators: signIn = {
    passwordField: '#ap_password',
    emailField: '#ap_email',
    signInBtn: "#signInSubmit",
    signImdb: ".auth-provider-text",
    validateLogin: ".imdb-header__account-toggle--logged-in",
    rememberMe: "[name='rememberMe']",
    errorMessage: ".a-alert-heading"
}

