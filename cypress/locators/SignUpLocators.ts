interface signUp {
    nameField: string,
    passwordField: string,
    emailField: string,
    checkPasswordField: string,
    createAccount: string,
    errorMessage: string
}

export const signUpLocators: signUp = {
    nameField: '#ap_customer_name',
    passwordField: '#ap_password',
    emailField: '#ap_email',
    checkPasswordField: '#ap_password_check',
    createAccount: "#continue",
    errorMessage: ".a-icon-alert"
}

