interface home {
    barBtns: string,
    trailer: string,
    movieName: string,
    countOfMovies: string
}

export const homeLocators: home = {
    barBtns: ".ipc-button__text",
    trailer: '.ipc-icon--play-arrow',
    movieName: ".lister-item-header",
    countOfMovies: ".imdb-header__watchlist-button-count"
}